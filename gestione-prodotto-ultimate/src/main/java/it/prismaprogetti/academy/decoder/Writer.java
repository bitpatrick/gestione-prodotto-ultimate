package it.prismaprogetti.academy.decoder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;

public interface Writer {
	
	/**
	 * imposta i nomi delle colonne
	 * @param nomeCampi
	 */
	public void setNomeCampi(Collection<String> nomeCampi);
	
	/**
	 * aggiunge i valori in maniera sequenziale alle colonn
	 */
	public void addValoriCampi(Collection<String> valoriCampi);
	
	/**
	 * scrivi su file
	 * @param file
	 * @throws FileNotFoundException 
	 * @throws IOException 
	 */
	public void print(File file) throws IOException;

}
