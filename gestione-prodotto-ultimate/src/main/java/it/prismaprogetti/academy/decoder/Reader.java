package it.prismaprogetti.academy.decoder;

import java.io.IOException;
import java.util.List;

import it.prismaprogetti.academy.model.Record;

/**
 * fornisce un modo per avere l'accesso alle info scritte dal writer e poterle utilizzare
 * @author patri
 *
 */
public interface Reader {
	
	public boolean hasNextRecord();
	
	public Record nextRecord();
	
	public List<Record> read() throws IOException;
	
}
