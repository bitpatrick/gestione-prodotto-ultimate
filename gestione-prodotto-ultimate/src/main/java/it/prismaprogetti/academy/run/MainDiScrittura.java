package it.prismaprogetti.academy.run;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import it.prismaprogetti.academy.decoder.Writer;
import it.prismaprogetti.academy.impl.WriterImpl;

public class MainDiScrittura {

	public static void main(String[] args) {
		
		String path = "C:\\Users\\patri\\git\\repositorygestioneprodottoultimate\\gestione-prodotto-ultimate\\src\\main\\resources\\data.txt";
		
		WriterImpl writer = new WriterImpl("***", "<<NULL>>");
		
		writer.setNomeCampi(new ArrayList<String>( Arrays.asList(
				"primaColonna",
				"secondaColonna",
				"terzaColonna"
				)));
		
		writer.addValoriCampi(new ArrayList<String>(Arrays.asList(
				"valorePrimoCampoRecord1",
				"valoreSecondoCampoRecord1",
				"valoreTerzoCampoRecord1"
				)));
		
		writer.addValoriCampi(new ArrayList<String>(Arrays.asList(
				"valorePrimoCampoRecord2",
				"valoreSecondoCampoRecord2",
				"valoreTerzoCampoRecord2"
				)));
		
		
		try {
			writer.print(new File(path));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
