package it.prismaprogetti.academy.model;

import java.util.ArrayList;
import java.util.List;

public class Record {

	private String nome;
	private List<Campo> campi = new ArrayList<>();
	private int indiceCampi = -1;

	public Record(String nome, List<Campo> campi) {
		super();
		this.nome = nome;
		this.campi = campi;
	}

	public String getNome() {
		return nome;
	}

	public List<Campo> getCampi() {
		return campi;
	}

	public Campo nextCampo() {

		return this.campi.get(++this.indiceCampi);
	}

	public boolean hasNextCampo() {

		if (this.indiceCampi + 1 >= this.campi.size() - 1) {
			return false;
		}

		return true;
	}

	public void resetIndiceCampi() {

		this.indiceCampi = -1;
	}

}
