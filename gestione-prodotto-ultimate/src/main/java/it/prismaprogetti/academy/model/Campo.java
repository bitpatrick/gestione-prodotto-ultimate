package it.prismaprogetti.academy.model;

/**
 * un Campo � formato da un nome (rappresenterebbe il nome della colonna) e un
 * valore ( � rappresentato dal valore occupato relativo a quella colonna )
 * 
 * @author patri
 *
 */
public class Campo {

	private final String nome;
	private String valore;

	private Campo(String nome) {
		super();
		this.nome = nome;
	}
	
	private Campo(String nome, String valore) {
		super();
		this.nome = nome;
		this.valore = valore;
	}

	public String getNome() {
		return nome;
	}

	public String getValore() {
		return valore;
	}

	public void setValore(String valore) {
		this.valore = valore;
	}

	public static Campo creaCampoConNome(String nomeCampo) {

		if (nomeCampo.isBlank()) {
			throw new IllegalStateException("Il nome del campo deve essere valorizzato");
		}

		return new Campo(nomeCampo);
	}
	
	public static Campo creaCopiaCampo(Campo campo) {
		
		return new Campo(campo.getNome(),campo.getValore());
	}
	
	public static Campo creaCampoConIntestazioneNotNull(String nome, String valore) {
		
		if ( nome.isBlank() ) {
			throw new IllegalStateException("il nome del campo � nullo");
		}
		
		return new Campo(nome, valore);
	}

	
	

}
