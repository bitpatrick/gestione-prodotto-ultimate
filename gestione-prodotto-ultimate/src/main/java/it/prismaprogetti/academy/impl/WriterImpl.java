package it.prismaprogetti.academy.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import it.prismaprogetti.academy.decoder.Writer;
import it.prismaprogetti.academy.model.Campo;
import it.prismaprogetti.academy.model.Record;

public class WriterImpl implements Writer {

	private final String separatoreDiRecord;
	/*
	 * 1 campo � composto dal nome del campo e 1 suo valore associato
	 */
	private List<Campo> campiIntestazione = new ArrayList<>();
	private final String valoreNullo;
	/*
	 * 1 record contiene 1 o piu campi
	 */
	private List<Record> records = new ArrayList<>();
	private int indiceRecords = 1;
	private boolean open = false;

	public WriterImpl(String separatoreDiRecord, String valoreNullo) {
		super();
		this.separatoreDiRecord = separatoreDiRecord;
		this.valoreNullo = valoreNullo;
	}

	public String getSeparatoreDiRecord() {
		return separatoreDiRecord;
	}

	public List<Campo> getCampiIntestazione() {
		return campiIntestazione;
	}

	public String getValoreNullo() {
		return valoreNullo;
	}

	public List<Record> getRecords() {
		return records;
	}

	/**
	 * metodo che imposta i nomi dei campi ( nomi colonne ) per i record che
	 * verranno inseriti
	 */
	@Override
	public void setNomeCampi(Collection<String> nomeCampi) {

		if (!this.campiIntestazione.isEmpty()) {
			throw new IllegalStateException("sono gi� stati inseriti dei nomi campo");
		}

		for (String nomeCampo : nomeCampi) {

			/*
			 * creo campo con un certo hash
			 */
			Campo campo = Campo.creaCampoConNome(nomeCampo);
			/*
			 * aggiungo quel campo a una lista che ha anche lei un determinato has
			 */
			this.campiIntestazione.add(campo);
		}

		this.open = true;

	}

	/**
	 * qui sto aggiungendo un record ogni volta che viene chiamato questo metodo
	 */
	@Override
	public void addValoriCampi(Collection<String> valoriCampi) {

		if (!this.open) {
			throw new IllegalStateException("devi impostare almeno un nome campo");
		}

		/*
		 * verifico che la grandezza di valori campi non ecceda quella dei nomi del
		 * campo
		 */
		if (valoriCampi.size() > this.campiIntestazione.size()) {
			throw new IllegalStateException("la grandezza dei valori inseriti eccede quella dei nomi dei campi");
		}

		/*
		 * associo i valori ai rispettivi nomi campo
		 */
		Iterator<String> iteratorValoriCampi = valoriCampi.iterator();
		int indiceNomiCampi = 0;
		Campo campo;

		while (iteratorValoriCampi.hasNext()) {
			/*
			 * prendo ogni singolo valore campo passato in input
			 */
			String valoreCampo = iteratorValoriCampi.next();
			/*
			 * associo questi valori passati in input rispettivamente ad ogni singolo campo
			 * intestazione pre impostato: prendo quel determinato campo con quell'hash
			 */
			campo = this.campiIntestazione.get(indiceNomiCampi);

			if (valoreCampo.isBlank()) {
				campo.setValore(this.valoreNullo);
			} else {
				campo.setValore(valoreCampo);
			}

			/*
			 * passo al campo successivo
			 */
			indiceNomiCampi++;
		}

		/*
		 * se presenti meno valori dei nomi campo allora riempio i valori restanti
		 * associati ai nomi campi orfani con il valore nullo impostato
		 */
		for (int j = 0; j < (this.campiIntestazione.size() - valoriCampi.size()); j++) {

			campo = this.campiIntestazione.get(indiceNomiCampi);
			campo.setValore(this.valoreNullo);
			indiceNomiCampi++;
		}

		this.records.add(new Record("record" + this.indiceRecords, this.clonaCampi()));

		this.indiceRecords++;
	}

	private List<Campo> clonaCampi() {

		List<Campo> campi = this.campiIntestazione;
		List<Campo> campiClonati = new ArrayList<>(campi.size());

		for (Campo campo : campi) {

			campiClonati.add(Campo.creaCopiaCampo(campo));

		}

		return campiClonati;
	}

	/**
	 * metodo che scrivi il file
	 */
	@Override
	public void print(File file) throws IOException {

		if (!this.open) {
			throw new IllegalStateException("devi impostare almeno un campo");
		}

		try (
				/*
				 * apro canale di comunicazione
				 */
				OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(file));
				/*
				 * applico decoratore
				 */
				BufferedWriter bw = new BufferedWriter(osw);) {

			/*
			 * ottengo dimensione dei campi creati
			 */
			int campiSize = this.campiIntestazione.size();

			/*
			 * scrivo l'intestazione del file: nomi dei campi
			 */
			for (int i = 0; i < (campiSize - 1); i++) {

				bw.write(this.campiIntestazione.get(i).getNome() + ",");
			}
			bw.write(this.campiIntestazione.get(campiSize - 1).getNome() + "\n");

			/*
			 * ciclo sui record
			 */
			for (Record record : this.records) {

				/*
				 * ciclo sui campi del record corrente e li scrivo su file
				 */
				for (Campo campo : record.getCampi()) {

					bw.write(campo.getValore() + "\n");
				}

				/*
				 * dopo che ho raggiunto la fine del record aggiungo il separatore
				 */
				bw.write(this.separatoreDiRecord + "\n");

			}

		}

	}

}
