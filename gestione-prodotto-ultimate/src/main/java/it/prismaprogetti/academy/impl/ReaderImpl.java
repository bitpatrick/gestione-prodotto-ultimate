package it.prismaprogetti.academy.impl;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import it.prismaprogetti.academy.decoder.Reader;
import it.prismaprogetti.academy.model.Campo;
import it.prismaprogetti.academy.model.Record;

public class ReaderImpl implements Reader {

	private List<Record> records = new ArrayList<>();
	private int indiceRecords = -1;
	private String path;
	private char separatore;
	private boolean tolleranza = false;

	public ReaderImpl(String path) {
		super();
		this.path = path;
	}

	@Override
	public boolean hasNextRecord() {

		if (this.indiceRecords + 1 >= this.records.size() - 1) {

			return false;
		}

		return true;
	}

	@Override
	public Record nextRecord() {

		return this.records.get(++indiceRecords);
	}

	@Override
	public List<Record> read() throws IOException {
		
		List<Record> records = new ArrayList<Record>();
		

		try ( 
				InputStreamReader isr = new InputStreamReader(new FileInputStream(this.path));
				BufferedReader br = new BufferedReader(isr);
			) {
			
			/*
			 * recupero intestazione
			 */
			String intestazioneString = br.readLine();
			String[] nomeCampiArray = intestazioneString.split(",");
			int numeroCampi = nomeCampiArray.length;
			
			String valoreCampo = null;
			
			Queue<String> valoriCampi = new ArrayDeque<>();
			
			int indiceRecord = 1;
			
			List<Campo> campi = new ArrayList<>();
			
			int contaCicli = 0;
			while ( (valoreCampo = br.readLine()) != null ) {
				
				contaCicli++;
				/*
				 * verifico se � fine record
				 */
				if ( valoreCampo.contains(String.valueOf(this.separatore) ) ) {
					
					/*
					 * passa ai dati del record successivo
					 */
					continue;
				}
				
				/*
				 * verifico se il numero dei cicli supera il numero dei campi
				 * ovvero, i campi eccedono le colonne
				 */
				if ( contaCicli > numeroCampi ) {
					
					System.out.println("numero di campi eccessivo");
					
					if ( this.tolleranza ) {
						
						System.out.println("applico salvataggio");
						//TODO
						
					} else {
						
						System.out.println("non applico salvataggio");
						//TODO
					}
					
				}
				
				/*
				 * ciclo sui nomi campi recuperati
				 */
				for ( int indiceCampo = 0; indiceCampo < nomeCampiArray.length; indiceCampo++) {
					
					/*
					 * recupero il nome del campo corrente
					 */
					String nome = nomeCampiArray[indiceCampo++];
					
					/*
					 * creo campo con il solo nome
					 */
					Campo campo = Campo.creaCampoConIntestazioneNotNull(nome, valoreCampo);
					campi.add(campo);
					
				}
				
				Campo campo = campiIntestazione.get(indiceRecords)
				
				valoriCampi.add(valoreCampo);
				
				
			}
			
	}
		
		
		return null;
	}
	

}
